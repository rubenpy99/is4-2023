<?php

define('tamano_matriz', 4);  // tamaño en duro de la matriz 4x4

function crearMatriz($n) {
    $matriz = [];
    for ($i = 0; $i < $n; $i++) {
        for ($j = 0; $j < $n; $j++) { 
            $matriz[$i][$j] = mt_rand(0, 9);  // valores aleatorios entre 0 y 9 
        }
    }
    return $matriz;
}

function imprimirMatriz($matriz) {
    $sum = 0;  
    echo "<table border='1'>";
    foreach ($matriz as $i => $fila) {
        echo "<tr>";
        foreach ($fila as $j => $valor) {
            echo "<td>" . $valor . "</td>";
            if ($i == $j) {  // si el numero de columna es igual al numero de fila es un valor de la diagonal principal
                $sum += $valor;
            }
        }
        echo "</tr>";
    }
    echo "</table>";
    return $sum;  // la suma de la diagonal principal
}

function mostrarSuma() {
    do {
        $matriz = crearMatriz(tamano_matriz);
        echo "Matriz:<br>";
        $sum = imprimirMatriz($matriz);
        echo "La suma de la DP es: $sum<br>";
    } while ($sum < 10 || $sum > 15);

    echo "Condición encontrada!!";
}

mostrarSuma();

?>
