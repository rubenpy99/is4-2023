<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" >
    <meta name="description" content="Mi segundo Script PHP">
    <title>Tabla de Productos</title>
    <style>
        table {
            border-collapse: collapse;
            width: 22%;
            margin: 20px auto;
            font-weight:bold ;
        }

        th {
            background-color: yellow; /* Título de la tabla en amarillo */
            color: black;
        }

        th, td {
            border: 1px solid #000;
            padding: 8px;
            text-align: center;
        }

        tr:nth-child(2) { /* Segunda fila en #d0cece */
            background-color: #d0cece;
            color: black;
        }

        tr:nth-child(even) { /* Filas pares en blanco */
            background-color: #ffffff;
            color: black;
        }

        tr:nth-child(odd) { /* Filas impares en #e2efda */
            background-color: #e2efda;
            color: black;
        }

        .titulo {
            background-color: yellow; /* Color amarillo */
            font-weight: bold;
        }

        .descripcion th {
            background-color: gray; /* Fila de descripciones en gris */
            font-weight: bold;
        }
    </style>
</head>
<body>
    <h1>Tabla de Productos</h1>
    
    <?php
    // Datos de productos
    $productos = array(
        array("Nombre", "Cantidad", "Precio Gs"),
        array("Coca Cola", 100, 4500),
        array("Pepsi", 30, 4800),
        array("Sprite", 20, 4500),
        array("Guaraná", 200, 4500),
        array("SevenUp", 24, 4800),
        array("Mirinda Naranja", 56, 4500),
        array("Mirinda Guaraná", 89, 4500),
        array("Fanta Naranja", 10, 4500),
        array("Fanta Piña", 2, 4500),
    );
    
    // Generar la tabla HTML
    echo '<table>';
    
    // Agregar la fila de título "Productos" que ocupa todas las columnas
    echo '<tr class="titulo"> <th colspan="3">Productos</th></tr>';
    
    foreach ($productos as $index => $fila) {
        if ($index === 0) {
            echo '<tr class="descripcion">';
        } else {
            echo '<tr>';
        }
        
        foreach ($fila as $valor) {
            echo '<td>' . $valor . '</td>';
        }
        echo '</tr>';
    }
    
    echo '</table>';
    ?>
</body>
</html>
