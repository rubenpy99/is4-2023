<?php
// Incluyendo el archivo de configuración
include_once 'config.php';

// Iniciando la sesión
session_start();

// Verificando si el formulario fue enviado
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $usuario = $_POST['usuario'];
    $password = $_POST['password'];

    try {
        // Preparando la consulta
        $stmt = $pdo->prepare('SELECT * FROM users WHERE nombre_usuario = :usuario');
        $stmt->execute(['usuario' => $usuario]);
        $user = $stmt->fetch();

        // Verificando si el usuario existe y la contraseña es correcta
        if ($user && password_verify($password, $user['password'])) {
            // Guardando los datos en la sesión
            $_SESSION['nombre'] = $user['nombre'];
            $_SESSION['apellido'] = $user['apellido'];
            echo "<div style='margin: 20px; padding: 20px; border: 1px solid #ddd;'>";
            echo "<h1>Bienvenido " . $user['nombre'] . " " . $user['apellido'] . "</h1>";
            echo "<br><a href='logout.php' style='display: block; margin-top: 20px; padding: 10px; border: 1px solid #ddd; text-align: center; width: 100px;'>Logout</a>";
            echo "</div>";
        } else {
            echo "<script>alert('Usuario o contraseña incorrecta');</script>";
            include 'login.html';  // Mostrando el formulario de login nuevamente
            file_put_contents('log.txt', "Intento fallido de login: $usuario\n", FILE_APPEND);
        }
    } catch (PDOException $e) {
        // Manejo de errores
        file_put_contents('log.txt', "Error al conectar con la base de datos: " . $e->getMessage() . "\n", FILE_APPEND);
        echo "<script>alert('Error al conectar con la base de datos.');</script>";
    }
}

?>
