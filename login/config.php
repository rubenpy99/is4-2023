<?php
$host = 'localhost';
$db = 'login';
$user = 'postgres';  // Reemplaza con tu usuario de PostgreSQL
$pass = 'newpassword';  // Reemplaza con tu contraseña de PostgreSQL
$port = 5432;        // Reemplaza con tu puerto de PostgreSQL si es diferente

try {
    $pdo = new PDO("pgsql:host=$host;port=$port;dbname=$db", $user, $pass, [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
    ]);
} catch (PDOException $e) {
    file_put_contents('log.txt', "Error al conectar con la base de datos: " . $e->getMessage() . "\n", FILE_APPEND);
    echo "<script>alert('Error al conectar con la base de datos.');</script>";
    exit();
}


