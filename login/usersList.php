<?php
include 'config.php';

$sql = 'SELECT * FROM users ORDER BY id ASC';
$stmt = $pdo->prepare($sql);
$stmt->execute();
$users = $stmt->fetchAll();
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Listado de Usuarios</title>
<style>
    table {
        width: 50%;
        margin: 20px auto;
        border-collapse: collapse;
    }
    th, td {
        padding: 8px 12px;
        border-bottom: 1px solid #ddd;
    }
    th {
        background-color: #f2f2f2;
    }
</style>
</head>
<body>

<table>
    <tr>
        <th>ID</th>
        <th>Nombre</th>
        <th>Apellido</th>
        <th>Nombre de Usuario</th>
        <th>Contraseña</th>
    </tr>
    <?php foreach ($users as $user): ?>
    <tr>
        <td><?php echo $user['id']; ?></td>
        <td><?php echo $user['nombre']; ?></td>
        <td><?php echo $user['apellido']; ?></td>
        <td><?php echo $user['nombre_usuario']; ?></td>
        <td><?php echo $user['password']; ?></td>
    </tr>
    <?php endforeach; ?>
</table>

</body>
</html>
